// console.log('hi')

// ES6 Updates

// Exponent Operator (**)
const firstNum = 8 ** 2;
console.log(firstNum)
// result: 64

// before ES6 update
const secondNum = Math.pow(8, 2)
console.log(secondNum);
// result: 64

/*
	Mini-Activity
		>> Using exponent operator get the cube of 5
		>> Store the result in a variable called getCube
		>> Print the result in the console browser
		>> send your outputs in Hangouts
*/

let getCube = 5 ** 3
console.log(getCube)
//result: 125

// Templates Literals

let name = "Tine";

// before the update
let message = ('Hello ' + name + '! ' + 'Welcome to programming');
console.log('Result from pre-template literals:')
console.log(message);

// ES6 updates
// this uses backticks(``) - string data type
message = (`Hello ${name}! Welcome to programming`)
console.log('Result from pre-template literals:')
console.log(message);


/*
	Mini
*/

let string1 = "Zuitt";
let string2 = "Coding";
let string3 = "Bootcamp";
let string4 = "teaches";
let string5 = "Javascript";
let string6 = "as a";
let string7 = "programming language";

let stringsss = `${string1} ${string2} ${string3} ${string4} ${string5} ${string6} ${string7}`
console.log(stringsss)

strings = (`${string1} ${string2} ${string3} ${string4} ${string5} ${string6} ${string7}`)
console.log(strings)

const interestRate = .1;
const principal = 1000;

console.log(`The interest in your savings account is :${principal * interestRate}`)
// result: 100

//  Array Destructing
/*
	Syntax:
		let / const = [variableNameA, variableNameB, ...] = arrayName
*/

const fullName = ["Juan", "Dela", "Cruz"];

//  before uupdate
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[2]} ${fullName[2]}! It's nice to meet you.`);

// ES6 Updates
const [firstName, middleName, lastName,] = fullName;
// const [, middleName, ] = fullName

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName} It's nice to meet you.`)


// ES6 updates
let pointGuards = ['Curry', 'Lillard', 'Paul', 'Irving']


// SOLUTION
// Using ES6

const [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = pointGuards

console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);


// console.log(pointGuards[0]);
// console.log(pointGuards[1]);
// console.log(pointGuards[2]);
// console.log(pointGuards[3]);

// Object Destructure
/*
	Syntax:
		let / const {propertyNameA, propertyNameB, ...} = objectNam
*/
// before updates
const person = {
	giveName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
}

console.log(person.giveName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.giveName} ${person.maidenName} ${person.familyName}! It was nice meeting you`)

// ES6 updates

const {giveName, maidenName, familyName} = person;

console.log(giveName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${giveName} ${maidenName} ${familyName}! It was nice meeting you`)

/*
	Mini-Activity
		>> Given the object below, destructure each property
		>> Create sentence variable will the following string message
			My Pokemon is <pokemon1Name>, It is level <levelOfPokemon>. It is a <typeOfPokemon> type.
*/

let pokemon1 = {
	characterName: "Charmander",
	level: 11,
	type: "fire"
}


// SOLUTION
// Using ES6

const {characterName, level, type} = pokemon1

let sentence2 = `My Pokemon is ${characterName}, It is levl ${level}. It is a ${type}! type`
console.log(sentence2)

// console.log(pokemon1.characterName);
// console.log(pokemon1.level);
// console.log(pokemon1.type);

// console.log(`My Pokemon is ${pokemon1.characterName}, It is levl ${pokemon1.level}. It is a ${pokemon1.type}! type`)

// destructing in function parameter

function getFullName({giveName, maidenName, familyName}){
	console.log(`${giveName} ${maidenName} ${familyName}`)
};
getFullName(person);

// Arrow Function
/*
	Syntax:
		const vairableName = (parameter) => {
			statement / code block
		}
*/

// before update

// function printFullname(firstName, middleName, lastName){
// 	console.log(`${firstName} ${middleName} ${lastName}`)
// };

// printFullname('John', 'D', 'Smith');

// ES6 Update

const printFullName = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`);
};

printFullName('John', 'D', 'Smith');

const students = ['John', 'Jane', 'Juan'];

// forEach with traditional function
// student.forEach(function(){

// });

// arrow function
students.forEach(student => {
	console.log(`${student} is a student`)
});


// implicit return with arrow function

students.forEach((students) => console.log(`${students} is a student.`));

// implicit return

// this is explicit return
// const add = (x, y) => {
// 	return x + y
// };

// let total = add(9, 18)
// console.log(total)
// result: 27

// with implicit return
const add = (x, y) => x + y

let total = add(27, 9);
console.log(total);

// Default function argument value
const greet = (name = 'User') => {
	return `Good day! ${name}.`
};

console.log(greet());

// traditional function

// same above

// function greet(name = 'User'){
// 	return `Good day! ${name}`
// };

// Class-Based Object Blueprints
/*
	Syntax:
		class className {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objecPropertyA;
				this.objectPropertyB = objecPropertyB;
			}
		}
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year - year;
	}
}

// Syntax:
	// let / const variableName = new className()
// const myCar =new Car();
let myCar = new Car();
console.log(myCar);
// result: Car{brand: undefined, name: undefined

myCar.brand = 'Ford'
myCar.name = 'Ranger Raptor'
myCar.year = 2021;

console.log(myCar);

const newCar = new Car("Toyota", "Vios", 2021);
console.log(newCar)